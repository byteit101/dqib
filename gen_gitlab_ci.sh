#!/bin/bash

function print_variant() {
    variant="$1"
    allow_failure="$2"
    cat <<EOF

build_$variant:
  stage: build
  tags:
    - docker
    - privileged
  script:
    - apt-get update
    - apt-get install -y --no-upgrade eatmydata
    - eatmydata apt-get install -y --no-upgrade apt-utils qemu-user-static qemu-system qemu-utils mmdebstrap debian-archive-keyring debian-ports-archive-keyring libguestfs-tools linux-image-amd64 tar
    - mkdir dqib_$variant
    - http_proxy="http://10.224.0.7:3142/" DIR=dqib_$variant DISK_SIZE=0 ./create.sh $variant
    - ls -hl dqib_$variant
  allow_failure: $allow_failure
  artifacts:
    name: "dqib_$variant"
    paths:
      - dqib_$variant
    expire_in: 1 year

convert_$variant:
  stage: convert
  tags:
    - shell
  dependencies:
    - build_$variant
  needs: ["build_$variant"]
  script:
    - virt-make-fs --format=qcow2 --size=10G --partition=gpt --type=ext4 --label=rootfs dqib_$variant/image.tar.gz dqib_$variant/image.qcow2
    - qemu-img convert -f qcow2 dqib_$variant/image.qcow2 -O qcow2 dqib_$variant/image2.qcow2
    - mv dqib_$variant/image2.qcow2 dqib_$variant/image.qcow2
    - rm dqib_$variant/image.tar.gz
    - ls -hl dqib_$variant
  allow_failure: $allow_failure
  artifacts:
    name: "dqib_$variant"
    paths:
      - dqib_$variant
    expire_in: 1 year
EOF
}

cat <<EOF
image: registry.gitlab.com/giomasce/dqib/testenv

variables:
  GIT_STRATEGY: fetch
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_DEPTH: "3"

before_script:
  - cat /proc/cpuinfo
  - cat /proc/meminfo

stages:
  - build
  - convert
EOF

for variant in i386-pc amd64-pc mips64el-malta armel-raspi2 armhf-virt arm64-virt ppc64el-pseries s390x-virt alpha-clipper hppa-hppa m68k-q800 powerpc-g3beige ppc64-pseries riscv64-virt sh4-r2d loong64-virt; do
    print_variant $variant "false"
done

for variant in sparc64-sun4u ; do
    print_variant $variant "true"
done
