# dqib - Debian quick image baker
# Copyright © 2019 Giovanni Mascellani <gio@debian.org>

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

FROM debian:unstable
ENV DEBIAN_FRONTEND=noninteractive

RUN printf "deb http://deb.debian.org/debian unstable main\ndeb-src http://deb.debian.org/debian unstable main\n" > /etc/apt/sources.list
RUN apt-get update && apt-get install -y eatmydata
RUN eatmydata apt-get update && eatmydata apt-get dist-upgrade -y
RUN eatmydata apt-get install -y apt-utils qemu-user-static qemu-system qemu-utils mmdebstrap debian-archive-keyring debian-ports-archive-keyring libguestfs-tools linux-image-amd64 tar binfmt-support

COPY create.sh fstab interfaces keys enable_binfmt.sh /root/
RUN /root/enable_binfmt.sh
